<x-app-layout>
<div class="toast">
        <div class="toast-content">
            <i class="fas fa-solid fa-check check"></i>

            <div class="message">
                <span class="text text-1">Exito</span>
                <span class="text text-2">Datos registrados correctamente</span>
            </div>
            </div>
            <i class="fa-solid fa-xmark close"></i>

            <div class="progress"></div>
        </div>
    <!-- <form action="{{route('profesion.store')}}" method="POST">
        @csrf
        <div>
            <label for="nombre">Nombre profesion</label>
            <input type="text" name="nombre">
        </div>

        <input type="submit" value="Registrar">
    </form> -->
    <div class="login-box">
        <h2>Registro Profesiones</h2>
        <form action="{{route('profesion.store')}}" method="POST">
            @csrf
            <div class="user-box">
                <input type="text" name="nombre" required="">
                <label for="nombre">Nombre Profesion</label>
            </div>
            
            <a href="#" onclick="document.forms[0].submit();">
                <span></span>
                <span></span>
                <span></span>
                <span></span><input id="button" type="submit" value="Registrar">
            </a>
        </form>
    </div>

</x-app-layout>


<script>
var button = document.getElementById("button"),
      toast = document.querySelector(".toast")
      closeIcon = document.querySelector(".close"),
      progress = document.querySelector(".progress");

      let timer1, timer2;

      button.addEventListener("click", () => {
        toast.classList.add("active");
        progress.classList.add("active");

        timer1 = setTimeout(() => {
            toast.classList.remove("active");
        }, 5000); //1s = 1000 milliseconds

        timer2 = setTimeout(() => {
          progress.classList.remove("active");
        }, 5300);
      });
      
      closeIcon.addEventListener("click", () => {
        toast.classList.remove("active");
        
        setTimeout(() => {
          progress.classList.remove("active");
        }, 300);

        clearTimeout(timer1);
        clearTimeout(timer2);
      });

</script>