<?php

namespace App\Http\Controllers;

use App\Models\Profesion;
use Illuminate\Http\Request;

class ProfesionController extends Controller
{
    public function index()
    {
        $profesiones = Profesion::all();

        return view('profesiones.index', [
            'profesiones' => $profesiones
        ]);
    }

    public function create()
    {
        return view('profesiones.create');
    }


    public function store(Request $request)
    {
        $input = $request->all();
        $profesion = Profesion::create($input);
        return  view('profesiones.show', [
            'profesion' => $profesion
        ]);
    }

    public function show(Profesion $profesion)
    {
        return view('profesiones.show',[
            'profesion'=>$profesion
        ]);
    }

    public function edit(Profesion $profesion)
    {
        return view('profesiones.edit',[
            'profesion'=>$profesion
        ]);
    }

    public function update(Request $request, Profesion $profesion)
    {
        $input = $request->all();
        $profesion->update($input);
        return  view('profesiones.show', [
            'profesion' => $profesion
        ]);
    }

    public function destroy(Profesion $profesion)
    {
        $profesion->delete();
        return redirect()->route('profesion.index');
    }
}
